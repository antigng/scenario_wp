# scenario

## 文件名格式

文件名|注释
-----|----
a01.txt|提取的可读文本，不包括字母和数字
a01_a.txt|提取的乱码文本，包括字母和数字
a01_b.txt|校对和修改之后的文本
a01.diff|a01.txt和a01_b的差异文件

我通过比较前两个文件，将字母和数字在a01.txt补全。

所有文件的编码都是UTF-8。

## 参考
- https://archive.today/2024.03.12-044216/https://www.bilibili.com/read/cv9058072/
- https://github.com/morkt/GARbro
- https://github.com/AtomCrafty/MajiroTools
- https://github.com/Artikash/Textractor